\ProvidesPackage{preamble}

\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{algpseudocode}
%\algrenewcommand\textproc{\forcemathtt}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{multirow}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usepackage{graphviz}
\usepackage{dot2texi}
\usepackage{tikz}
\usepackage{pgfplots}
\pgfplotsset{compat=1.14}
\usetikzlibrary{shapes,arrows}
\usepackage{color}
\usepackage[hyphens]{url}
\usepackage{hyperref}
\usepackage{import}
\usepackage[framemethod=TikZ]{mdframed}
\usepackage{macros}
\usepackage{float}
\usepackage{appendix}
\usepackage{pifont}
\usepackage{newunicodechar}
\usepackage{wasysym}
\usepackage{subcaption}
\captionsetup{compatibility=false}

\usepackage{caption}
\usepackage{pstricks,pst-node}
\usepackage{auto-pst-pdf}
\usepackage{calc}
\usepackage{pgffor}
\usepackage{xparse}
\usepackage{expl3}
\usepackage[nomessages]{fp}
\usepackage{etoolbox}
\usepackage{xifthen}
\usepackage{xspace}
\usepackage{notation}
\usepackage{macros}

% PG: Margin comments were placed oddly for me, and this saved the day (taken from
% http://mirror.hmc.edu/ctan/macros/latex/contrib/todonotes/todonotes.pdf)
%\setlength{\marginparwidth}{2cm}

% Define a "math hyphen"
\mathchardef\mhyphen="2D

% Define lay summary environment, copying acknowledgements environment from
% infthesis.cls
\newenvironment{laysummary}
   {\renewcommand{\abstractname}{Lay Summary}\begin{mainabs}}
   {\end{mainabs}\renewcommand{\abstractname}{Abstract}}

% Define tick and ex
\newunicodechar{✓}{\ding{51}}
\newunicodechar{✗}{\ding{55}}

% indentation for algorithm
\algdef{SE}[SUBALG]{Indent}{EndIndent}{}{\algorithmicend\ }%
\algtext*{Indent}
\algtext*{EndIndent}

% Command for comments by Aggelos and Orfeas
\def\showauthnotes{1}
\ifthenelse{\showauthnotes=1}
{
\newcommand{\authnote}[3]{{ \footnotesize \bf{#1[#2: #3]~}}}
%\newcommand{\authnote}[2]{{ \tt {[#1: #2]~}}}
}
{ \newcommand{\authnote}[2]{} }
\newcommand{\aggelos}[1]{\authnote{\color{magenta}}{Aggelos}{#1}}
\newcommand{\orfeas}[1]{\authnote{\color{blue}}{Orfeas}{#1}}

% code options with \lstlisting
\lstset{frame=tb,
  showstringspaces=false,
  columns=flexible,
  basicstyle=\ttfamily,
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3,
  escapeinside={(*@}{@*)},
  frame=n
}
\lstdefinestyle{numbers}{numbers=left, stepnumber=1, numberstyle=\tiny, numbersep=10pt}
\let\origthelstnumber\thelstnumber
\makeatletter
\newcommand*\Suppressnumber{%
  \lst@AddToHook{OnNewLine}{%
    \let\thelstnumber\relax%
     \advance\c@lstnumber-\@ne\relax%
    }%
}

\newcommand*\Reactivatenumber{%
  \lst@AddToHook{OnNewLine}{%
   \let\thelstnumber\origthelstnumber%
   \advance\c@lstnumber\@ne\relax}%
}

% bitcoin character
\def\bitcoin{%
  \leavevmode
  \vtop{\offinterlineskip %\bfseries
    \setbox0=\hbox{B}%
    \setbox2=\hbox to\wd0{\hfil\hskip-.03em
    \vrule height .3ex width .15ex\hskip .08em
    \vrule height .3ex width .15ex\hfil}
    \vbox{\copy2\box0}\box2}}

% fontsize between \LARGE and \huge
\makeatletter
\newcommand\semihuge{\@setfontsize\semihuge{19.22}{23.88}}
\makeatother

% UC environments and commands
\newenvironment{ucEnv}[1]{\begin{mdframed}{\centering\textbf{#1}\par}\small\vspace{\baselineskip}\noindent}{\end{mdframed}}
\newenvironment{functionality}[1]{\begin{ucEnv}{Functionality #1}}{\end{ucEnv}}
\newenvironment{protocol}[1]{\begin{ucEnv}{Protocol #1}}{\end{ucEnv}}
\newcommand{\funcsection}[1]{\vspace{0.7em}\hrule\vspace{1.5em}\par\noindent\emph{#1:}\vspace{0.5em}}

% environment for separate proof
\newenvironment{sepproof}[1]{\noindent \textbf{#1}}{$\square$ \smallskip \ \\}

% environment for proof sketch
\ifelseieee
  {\newenvironment{proofsketch}[1][Proof Sketch]{\begin{IEEEproof}[#1]}{\end{IEEEproof}}}
  {\newenvironment{proofsketch}{\noindent \textit{Proof Sketch.}}{$\square$ \smallskip \ \\}}

% page style set to plain
\pagestyle{plain}

% bibliography style set to splncs
\bibliographystyle{splncs}

% PDF bookmarks
\definecolor{darkblue}{rgb}{0.0,0.0,0.3}
\hypersetup{colorlinks,breaklinks,
    linkcolor=darkblue,urlcolor=darkblue,
    anchorcolor=darkblue,citecolor=darkblue}

% custom figures counter
\newcounter{figcount}
\newcommand{\figlabel}[1]{\refstepcounter{figcount}\arabic{figcount}\label{#1}}

% make contents green if \togreen is true
\newcommand{\greenit}[1]{%
  \ifthenelse{\equal{\togreen}{true}}{%
    \textcolor{green}{#1}%
  }{#1}%
}

% highlight code run by honest message recipient
\newcommand{\remotecode}[1]{%
  \textcolor{red}{#1}%
}

% highlight code run by trusted message recipient
\newcommand{\trustedcode}[1]{%
  \textcolor{blue}{#1}%
}

% argmin and argmax commands
\DeclareMathOperator*{\argmax}{argmax}
\DeclareMathOperator*{\argmin}{argmin}

% macros for symbols
\newcommand{\alice}{\ensuremath{\mathit{Alice}}\xspace}
\newcommand{\bob}{\ensuremath{\mathit{Bob}}\xspace}
\newcommand{\charlie}{\ensuremath{\mathit{Charlie}}\xspace}
\newcommand{\dave}{\ensuremath{\mathit{Dave}}\xspace}
\newcommand{\eve}{\ensuremath{\mathit{Eve}}\xspace}
\newcommand{\frank}{\ensuremath{\mathit{Frank}}\xspace}
\newcommand{\george}{\ensuremath{\mathit{George}}\xspace}
\newcommand{\ledger}{\ensuremath{\mathcal{G}_{\mathrm{Ledger}}}\xspace}
\newcommand{\perfectledger}{\ensuremath{\mathcal{F}_{\mathrm{PerfectL}}}\xspace}
\newcommand{\perfectprot}{\ensuremath{\Pi_{\mathrm{PerfectL}}}\xspace}
\newcommand{\fpaynet}{\ensuremath{\mathcal{F}_{\mathrm{PayNet}}}\xspace}
\newcommand{\func}{\ensuremath{\mathcal{F}}\xspace}
\newcommand{\fchan}{\ensuremath{\mathcal{\green{G}}_{\mathrm{Chan}}}\xspace}
\newcommand{\fchanhat}{\ensuremath{\hat{\mathcal{F}}_{\mathrm{Chan}}}\xspace}
\newcommand{\gtrust}{\ensuremath{\mathcal{G}_{\mathrm{Trust}}}\xspace}
\newcommand{\prot}{\ensuremath{\Pi}\xspace}
\newcommand{\pchan}{\ensuremath{\Pi_{\mathrm{Chan}}}\xspace}
\newcommand{\pchanhat}{\ensuremath{\hat{\Pi}_{\mathrm{Chan}}}\xspace}
\newcommand{\simulator}{\ensuremath{\mathcal{S}}\xspace}
\newcommand{\adversary}{\ensuremath{\mathcal{A}}\xspace}
\newcommand{\environment}{\ensuremath{\mathcal{E}}\xspace}
\newcommand{\perfectenv}{\ensuremath{\environment_{\mathrm{PL}}}\xspace}
\newcommand{\perfectadv}{\ensuremath{\adversary_{\mathrm{PL}}}\xspace}
\newcommand{\pssubmits}{\ensuremath{p^{\simulator}_{\mathrm{submits}}}}
\newcommand{\ppsubmits}{\ensuremath{p^{\perfectprot}_{\mathrm{submits}}}}
\newcommand{\ppfetches}{\ensuremath{p^{\perfectprot}_{\mathrm{fetches}}}}
\NewDocumentCommand{\pk}{m}{\textit{pk}_{#1}}
\NewDocumentCommand{\sk}{m}{\textit{sk}_{#1}}
\NewDocumentCommand{\subbalance}{m}{\forcemathtt{balance}_{#1}}
\NewDocumentCommand{\coins}{m}{\forcemathtt{coins}_{#1}}
\NewDocumentCommand{\itistate}{o}{
  \IfNoValueTF{#1}
    {\ensuremath{\textit{State}}\xspace}
    {\ensuremath{\textit{State}_{#1}}\xspace}%
}

% https://tex.stackexchange.com/a/448386
\newcommand*\phantomrel[1]{\mathrel{\phantom{#1}}}

% https://tex.stackexchange.com/a/336650
\algnewcommand{\IfThenElse}[3]{% \IfThenElse{<if>}{<then>}{<else>}
  \State \algorithmicif\ #1\ \algorithmicthen\ #2\ \algorithmicelse\ #3}
\algnewcommand{\IfThen}[2]{% \IfThen{<if>}{<then>}
  \State \algorithmicif\ #1\ \algorithmicthen\ #2}

% C-like /* comment */
\algnewcommand{\StarComment}[1]{\textcolor{gray}{/*#1*/}}

% Puff of Steem symbols
\newcommand{\gfunc}{\mathcal{G}_{\mathrm{Feed}}}
\newcommand{\gfuncname}{\mathrm{Feed}}
\newcommand{\honeststr}{\Pi_{\mathrm{honest}}}
\newcommand{\honest}{\mathrm{honest}}
\newcommand{\post}{P}
\newcommand{\player}{u}
\newcommand{\like}{l}
\newcommand{\postlist}{\mathcal{P}}
\newcommand{\playerlist}{\mathcal{U}}
\newcommand{\postlen}{M}
\newcommand{\playerlen}{N}
\newcommand{\env}{\mathcal{E}}
\newcommand{\pvs}{\mathcal{S}}
\newcommand{\stpowvec}{\mathrm{\textbf{SP}}}
\newcommand{\votpowvec}{\mathrm{\textbf{VP}}}
\newcommand{\votpowvecreg}{\mathrm{\textbf{VP}reg}}
\newcommand{\stpow}{\mathrm{SP}}
\newcommand{\votpow}{\mathrm{VP}}
\newcommand{\rounds}{R}
\newcommand{\round}{r}
\newcommand{\attspan}{\mathrm{attSpan}}
\newcommand{\pid}{\mathrm{pid}}
\newcommand{\result}{\mathrm{res}}
\newcommand{\regen}{\mathrm{regen}}
\newcommand{\execpat}{\mathrm{ExecPat}}
\newcommand{\voterounds}{\mathrm{voteRounds}}
\newcommand{\idsc}[1]{\mathrm{idealSc}\left(#1\right)}
\newcommand{\rvp}{\mathrm{rVP}}
\newcommand{\strongpost}{\mathrm{strongPost}}
\newcommand{\weakpost}{\mathrm{weakPost}}
\newcommand{\nullpost}{\mathrm{nullPost}}
\newcommand{\bestpost}[1]{\mathrm{bestPost}\left(#1\right)}
\newcommand{\nichepost}[1]{\mathrm{nichePost}\left(#1\right)}
\newcommand{\votenum}{V}
\newcommand{\nichepostnum}{V}

% Lightning symbols
\newcommand{\tochain}{\ensuremath{\left(2 + r\right)\windowSize}}
\newcommand{\state}{\variable{state}}
\newcommand{\forcemathtt}[1]{\ensuremath{\mathtt{#1}}}
\newcommand\scriptoverset[2]{\overset{\scriptstyle #1\mathstrut}{#2}}

% Elmo macros
\NewDocumentCommand{\fchansup}{m}{\mathcal{\green{G}}_{\mathrm{Chan}}^{#1}\xspace}
\NewDocumentCommand{\pchansup}{m}{\Pi_{\mathrm{Chan}}^{#1}\xspace}

% Voting macros
\newcommand{\votebox}{\ensuremath{\mathcal{G}_{\mathrm{VoteBox}}^{\mathcal{P}}}\xspace}
\newcommand{\decfunc}{\ensuremath{\mathsf{getDecryptors}_{n, B, \mathcal{P}}}}
\newcommand{\fproof}{\ensuremath{\mathcal{F}_{\mathrm{\green NIZK}}}\xspace}
\newcommand{\fcrs}{\ensuremath{\mathcal{F}_{\mathrm{CRS}}^{\mathcal{P}}}\xspace}
\newcommand{\pvote}{\ensuremath{\Pi_{\mathrm{vote}}^{B, n, t}}\xspace}
\newcommand{\fvote}{\ensuremath{\mathcal{F}_{\mathrm{vote}}^{B, n, t}}\xspace}

\newcommand{\togreen}{true}
