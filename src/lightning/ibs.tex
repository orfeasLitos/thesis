\subsubsection{Identity Based Signatures}
\label{sec:ibs}
In the Lightning Network specification, a custom scheme for deriving three
new keys per channel update is used. Its syntax and security aims closely
match those of previously studied Identity Based Signature
schemes~\cite{ibsshamir,ibspaterson}, thus we use the latter to abstract
away the complexity of the construction and highlight the security
requirements it satisfies. Our version augments the scheme with explicit
verification keys, which are generated together with the signing keys.
Furthermore we introduce a new key derivation algorithm which, on input of
the public parameters $mpk$ and a label $l$, returns the verification key
$pk_l$. Such a modified IBS scheme provides 5 algorithms:
\begin{itemize}
  \item $(mpk, msk) \leftarrow \textsc{Setup}(1^k)$: master keypair
  generation
  \item $(pk_l, sk_l) \leftarrow \textsc{KeyDer}(mpk, msk, l)$: keypair
  derivation with label $l$
  \item $pk_l \leftarrow \textsc{PubKeyDer}(mpk, l)$: verification key
  derivation with label $l$
  \item $\sigma \leftarrow \textsc{SignIBS}(m, sk_l)$: signature generation
  with signing key $sk_l$
  \item $\{0, 1\} \leftarrow \textsc{VerifyIBS}(\sigma, m, pk_l)$: signature
  verification
\end{itemize}
Observe that $mpk$ is not part of the input to \textsc{SignIBS} and
\textsc{VerifyIBS}. In our case, this input is not needed. In fact, because
of the underlying similarity of these two algorithms with their counterparts
from standard Digital Signatures, such an input would rather complicate the
exposition.

We demand that the following holds for a scheme to have correctness:
\begin{itemize}
  \item $\forall k \in \mathbb{N}, l \in \mathcal{L},$ \\
  $\Pr[(mpk, msk) \gets \textsc{Setup}(1^k),$ \\
  $(pk_1, sk_1) \gets \textsc{KeyDer}\left(mpk, msk, l\right),$ \\
  $pk_2 \gets \textsc{PubKeyDer}\left(mpk, l\right),$ \\
  $pk_1 = pk_2] = 1$

  \item $\forall k \in \mathbb{N}, m \in \mathcal{M},$ \\
  $\Pr[(mpk, msk) \gets \textsc{Setup}(1^k),$ \\
  $(pk, sk) \gets \textsc{KeyDer}\left(mpk, msk, l\right),$ \\
  $\textsc{VerifyIBS}(\textsc{SignIBS}(m, sk), m, pk) = 1] = 1$
\end{itemize}

\greenit{Let $\textsc{IBSalgs} = \{\textsc{Setup}, \textsc{KeyDer}, \textsc{PubKeyDer},
\textsc{SignIBS}, \textsc{VerifyIBS}\}$.}

\begin{center}
  \begin{gamebox}{$\mathsf{IBS \mhyphen EUF \mhyphen
  CMA}^{\adversary}\left(1^k\greenit{, \textsc{IBSalgs}}\right)$}
    \begin{algorithmic}[1]
      \State $(mpk, msk) \gets \textsc{Setup}(1^k)$
      \State $i, j \gets 0$
      \State $(\mathtt{aux}_0, \mathrm{response}) \gets
      \adversary(\textsc{init}, mpk)$
      \While{response can be parsed as $(m, l)$ or $l$}
        \If{response can be parsed as $(m, l)$}
          \State $i \gets i + 1$
          \State store $(m, l)$ as $(m, l)_i$
          \State $(pk, sk) \gets \textsc{KeyDer}(mpk, msk, l)$
          \State $\sigma \gets \textsc{SignIBS}(m, sk)$
          \State $(\mathtt{aux}_{i+j}, \mathrm{response}) \gets
          \adversary(\textsc{signature}, \mathtt{aux}_{i+j-1}, \sigma)$
        \Else \ \Comment{response can be parsed as $l$}
          \State $j \gets j + 1$
          \State store $l$ as $l_j$
          \State $(pk, sk) \gets \textsc{KeyDer}(mpk, msk, l)$
          \State $(\mathtt{aux}_{i+j}, \mathrm{response}) \gets
          \adversary(\textsc{keypair}, \mathtt{aux}_{i+j-1}, (pk, sk))$
        \EndIf
      \EndWhile
      \State parse response as $(m^*, l^*, \sigma^*)$
      \If{$(m^*, l^*) \notin \{(m, l)_1, \dots, (m, l)_i\} \wedge l^* \notin
      \{l_1, \dots, l_j\} \wedge \textsc{VerifyIBS}(\sigma^*, m^*,
      \textsc{PubKeyDer}(mpk, l^*)) = 1$}
        \State \Return 1
      \Else
        \State \Return 0
      \EndIf
    \end{algorithmic}
  \end{gamebox}
  \captionof{figure}{$\mathsf{IBS \mhyphen EUF \mhyphen
  CMA}^{\adversary}\left(1^k, \textsc{IBSalgs}\right)$ game}
  \label{game:ibs}
\end{center} \ \\
\begin{definition}
  \label{def:ibs:secure}
  An Identity Based Signatures scheme \greenit{\textsc{IBSalgs}} is
  \emph{\textsf{IBS-EUF-CMA}-secure} if
  \begin{gather*}
    \forall k \in \mathbb{N}, \forall \adversary \in \text{PPT},
    \Pr\left[\mathsf{IBS \mhyphen EUF \mhyphen CMA}^{\adversary}\left(1^k\greenit{,
    \textsc{IBSalgs}}\right) = 1\right] = \mathit{negl}\left(k\right)\greenit{,}
    \text{equivalently} \\
    \forall k \in \mathbb{N}, \mathrm{E \mhyphen ibs}(k) =
    \mathit{negl}\left(k\right) \enspace, \\
    \text{where } \mathrm{E \mhyphen ibs}(k) = \underset{\adversary \in
    \text{PPT}}{\sup}\left\{\Pr[\mathsf{IBS \mhyphen EUF \mhyphen
    CMA}^{\adversary}\left(1^k\greenit{, \textsc{IBSalgs}}\right) = 1]\right\} \enspace.
  \end{gather*}
\end{definition}

We here define the particular construction for Identity Based Signatures
used in LN and prove its security. \greenit{Let \textsc{LN-IBS} be its 5 algorithms; the
parameters are hard-coded in the algorithms.}

Parameters: hash function $\mathcal{H}$, group generator $G$
\begin{algorithmic}[0]
  \State \textsc{Setup}($1^k$):
  \Indent
    \State \Return \greenit{\textsc{KeyGen}($1^k$)}
  \EndIndent
\end{algorithmic}

\begin{algorithmic}[0]
  \State \textsc{KeyDer}($mpk, msk, l$):
  \Indent
    \State $pk \gets mpk + \mathcal{H}\left(l \concat mpk\right) \cdot G$
    \State $sk \gets msk + \mathcal{H}\left(l \concat mpk\right)$
    \State \Return $(pk, sk)$
  \EndIndent
\end{algorithmic}

\begin{algorithmic}[0]
  \State \textsc{PubKeyDer}($mpk, l$):
  \Indent
    \State \Return $mpk + \mathcal{H}\left(l \concat mpk\right) \cdot G$
  \EndIndent
\end{algorithmic}

\begin{algorithmic}[0]
  \State \textsc{SignIBS}($m, sk_l$):
  \Indent
    \State \Return \textsc{SignDS}($m, sk_l$)
  \EndIndent
\end{algorithmic}

\begin{algorithmic}[0]
  \State \textsc{VerifyIBS}($\sigma, m, pk_l$):
  \Indent
    \State \Return \textsc{VerifyDS}($\sigma, m, pk_l$)
  \EndIndent
\end{algorithmic}

\begin{lemma}
  \label{lemma:ibs}
  The construction above is \textsf{IBS-EUF-CMA}-secure in the Random Oracle
  model under the assumption that the underlying signature scheme is
  strongly \textsf{EUF-CMA}-secure and the range of the Random Oracle
  coincides with that of the underlying signature scheme signing keys.
\end{lemma}

\begin{proof}
  Let $k \in \mathbb{N}, \mathcal{B}$ PPT algorithm such that
  \begin{equation*}
    \Pr\left[\mathsf{IBS \mhyphen EUF \mhyphen
    CMA}^{\mathcal{B}}\left(1^k\greenit{, \textsc{LN-IBS}}\right) = 1\right] = a
    > \mathrm{negl}\left(k\right) \enspace.
  \end{equation*}
  We construct a PPT distinguisher \adversary{}
  (Fig.~\ref{proof:ibs:distinguisher}) such that
  \begin{equation*}
    \Pr\left[\mathsf{EUF \mhyphen CMA}^{\adversary}\left(1^k\right) =
    1\right] > \mathrm{negl}\left(k\right)
  \end{equation*}
  that breaks the assumption, thus proving Lemma~\ref{lemma:ibs}.

  \ \\\begin{center}
    \begin{algobox}{$\adversary\left(vk\right)$}
      \begin{algorithmic}[1]
        \State $k \overset{\$}{\gets} U\left[1, T\left(\mathcal{B}\right) +
        T\left(\adversary\right)\right]$
        \Comment{$T\left(M\right)$ is the maximum running time of $M$}
        \Indent
          \State Random Oracle: for every first-seen query $q$ from
          $\mathcal{B}$ set $\mathcal{H}\left(q\right)$ to a random value
          \State \Return $\mathcal{H}\left(q\right)$ to $\mathcal{B}$
        \EndIndent
        \State $\left(mpk, msk\right) \gets
        \textsc{Setup}\left(1^k\right)$
        \Indent
          \State Random Oracle: Let $q$ be the $k$th first-seen query from
          $\mathcal{B}$ or \adversary:
          \If{$q = \left(l \concat mpk\right)$ \greenit{for some $l \in \mathcal{L}$}}
            \State set $\mathcal{H}\left(l \concat mpk\right)$ to $\left(vk
            - mpk\right) \cdot G^{-1}$
          \Else
            \State set $\mathcal{H}\left(q\right)$ to a random value
          \EndIf
          \State \Return $\mathcal{H}\left(q\right)$ to $\mathcal{B}$ or
          \adversary
        \EndIndent
        \State $i \gets 0$
        \State $j \gets 0$
        \State $\left(\mathtt{aux}_0, \mathrm{response}\right) \gets
        \mathcal{B}\left(\textsc{init}, mpk\right)$
        \While{response can be parsed as $(m, l)$ or $l$}
          \If{response can be parsed as $(m, l)$}
            \State $i \gets i + 1$
            \State store $(m, l)$ as $(m, l)_i$
            \State $(pk, sk) \gets \textsc{KeyDer}(mpk, msk, l)$
            \State $\sigma \gets \textsc{SignIBS}(m, sk)$
            \State $\left(\mathtt{aux}_{i+j}, \mathrm{response}\right) \gets
            \mathcal{B}\left(\textsc{signature}, \mathtt{aux}_{i+j-1},
            \sigma\right)$
          \Else \ \Comment{response can be parsed as $l$}
            \State $j \gets j + 1$
            \State store $l$ as $l_j$
            \State $(pk, sk) \gets \textsc{KeyDer}(mpk, msk, l)$
            \State $\left(\mathtt{aux}_{i+j}, \mathrm{response}\right) \gets
            \mathcal{B}\left(\textsc{keypair}, \mathtt{aux}_{i+j-1}, (pk,
            sk)\right)$
          \EndIf
        \EndWhile
        \State parse response as $\left(m^*, l^*, \sigma^*\right)$
        \If{$vk = \textsc{PubKeyDer}(mpk, l^*) \wedge \mathcal{B}$ wins the
        \textsf{IBS-EUF-CMA} game} \Comment{\adversary{} won the
        \textsf{EUF-CMA} game}
        \label{proof:ibs:distinguisher:won}
          \State \Return $\left(m^*, \sigma^*\right)$
        \Else
          \State \Return \textsc{fail}
        \EndIf
      \end{algorithmic}
    \end{algobox}
    \captionof{figure}{Reduction of \textsf{IBS-EUF-CMA} attacker to
    \textsf{EUF-CMA} attacker}
    \label{proof:ibs:distinguisher}
  \end{center} \ \\

  Let $Y$ be the range of the random oracle. The modified random oracle used
  in Fig.~\ref{proof:ibs:distinguisher} is indistinguishable from the
  standard random oracle by PPT algorithms since the statistical distance of
  the standard random oracle from the modified one is at most
  $\frac{1}{2|Y|} < \mathit{negl}\left(k\right)$ as they differ in at most
  one element.

  Let $E$ denote the event in which neither \textsc{KeyDer}($mpk, msk, l^*$)
  nor \textsc{PubKeyDer}($mpk, l^*$) is invoked. In that case the value
  $\mathcal{H}\left(l \concat mpk\right)$ is decided after $\mathcal{B}$
  terminates (Fig.~\ref{proof:ibs:distinguisher},
  line~\ref{proof:ibs:distinguisher:won}) and thus
  \begin{equation}
    \begin{gathered}
      \Pr[vk \in \textsc{KeyDer}\left(mpk, msk, l^*\right) \vee \\
      vk = \textsc{PubKeyDer}\left(mpk, l^*\right) | E] <
      \mathit{negl}\left(k\right) \Rightarrow \\
      \Pr[(vk \in \textsc{KeyDer}\left(mpk, msk, l^*\right) \vee \\
      vk = \textsc{PubKeyDer}\left(mpk, l^*\right)) \wedge E] <
      \mathit{negl}\left(k\right) \Rightarrow \\
      \Pr\left[vk = \textsc{PubKeyDer}\left(mpk, l^*\right) \wedge E\right]
      < \mathit{negl}\left(k\right) \enspace.
    \end{gathered}
    \label{proof:ibs:nocomb}
  \end{equation}
  It is
  \begin{gather*}
    \left(\mathcal{B} \text{ wins}\right) \rightarrow \left(vk =
    \textsc{PubKeyDer}\left(mpk, l^*\right)\right) \Rightarrow \\
    \Pr\left[\mathcal{B} \text{ wins}\right] \leq \Pr\left[vk =
    \textsc{PubKeyDer}\left(mpk, l^*\right)\right] \Rightarrow \\
    \Pr\left[\mathcal{B} \text{ wins} \wedge E\right] \leq \Pr\left[vk =
    \textsc{PubKeyDer}\left(mpk, l^*\right) \wedge E\right]
    \overset{\left(\ref{proof:ibs:nocomb}\right)}{\Rightarrow} \\
    \Pr\left[\mathcal{B} \text{ wins} \wedge E\right] <
    \mathit{negl}\left(k\right) \enspace.
  \end{gather*}

  But we know that
  \begin{equation*}
    \Pr\left[\mathcal{B} \text{ wins}\right] = \Pr\left[\mathcal{B} \text{ wins}
    \wedge E\right] + \Pr\left[\mathcal{B} \text{ wins} \wedge \neg E\right]
    \text{ and } \Pr\left[\mathcal{B} \text{ wins}\right] = a
  \end{equation*}
  by the assumption, thus
  \begin{equation}
    \label{proof:ibs:nohash}
    \Pr\left[\mathcal{B} \text{ wins} \wedge \neg E\right] > a -
    \mathit{negl}\left(k\right) \enspace.
  \end{equation}

  We now focus at the event $\neg E$. Let $F$ the event in which the call of
  to \textsc{KeyDer}($mpk, msk, l^*$) or \textsc{PubKeyDer}($mpk, l^*$)
  results in the $k$th
  invocation of the Random Oracle. Since $k$ is chosen uniformly at random
  and using Proposition~\ref{prop:distrib}, it is
  $\Pr\left[F | \neg E\right] = \frac{1}{T\left(\mathcal{B}\right) +
  T\left(\adversary\right)}$. Observe that $\Pr\left[F | E\right] = 0
  \Rightarrow \Pr\left[F\right] = \Pr\left[F | \neg E\right] =
  \frac{1}{T\left(\mathcal{B}\right) + T\left(\adversary\right)}$.

  In the case where the event $\left(F \wedge \mathcal{B} \text{ wins}
  \wedge \neg E\right)$ holds, it is
  \begin{equation*}
    \textsc{PubKeyDer}\left(mpk, l^*\right) = mpk + \mathcal{H}\left(l^*
    \concat mpk\right) \cdot G = mpk + (vk - mpk) \cdot G^{-1} \cdot G = vk
  \end{equation*}
  Since $F$ holds, the $k$th invocation of the Random Oracle queried
  $\mathcal{H}\left(l^* \concat mpk\right)$.
  Therefore it is $\textsc{PubKeyDer}\left(mpk, l^*\right) = vk$. This means
  that the verification is successful, i.e., $\textsc{VerifyIBS}\left(\sigma^*,
  m^*, vk\right) = 1$. We conclude that, if $\left(F \wedge
  \mathcal{B} \text{ wins} \wedge \neg E\right)$, \adversary{} wins the
  \textsf{EUF-CMA} game. A final observation is that the probability that
  the events $\left(\mathcal{B} \text{ wins} \wedge \neg E\right)$ and $F$
  are almost independent, thus
  \begin{gather*}
    \Pr\left[F \wedge \mathcal{B} \text{ wins} \wedge \neg E\right] =
    \Pr\left[F\right] \Pr\left[\mathcal{B} \text{ wins} \wedge \neg E\right]
    \pm \mathit{negl}\left(k\right)
    \overset{\left(\ref{proof:ibs:nohash}\right)}{=} \\
    \frac{a - \mathit{negl}\left(k\right)}{T\left(\mathcal{A}\right) +
    T\left(\mathcal{B}\right)} \pm \mathit{negl}\left(k\right) >
    \mathit{negl}\left(k\right)
  \end{gather*}
\end{proof}
