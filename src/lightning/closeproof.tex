\ \\
\begin{center}
  \begin{simulatorbox}{$\simulator$}
    Like $\simulator{}_{\mathrm{LN - Reg - Open - Pay}}$. Differences:
    \begin{algorithmic}[1]
      \State Upon receiving (\textsc{closeChannel}, \forcemathtt{receipt},
      \textit{tid}, \alice) from \fpaynet:
      \Indent
        \State simulate Fig.~\ref{alg:protocol:close} receiving
        (\textsc{closeChannel}, \forcemathtt{receipt}, \textit{tid}) with \alice's
        ITI
      \EndIndent
      \Statex

      \State every time \forcemathtt{closedChannels} of \alice{} is updated with
      data from a \forcemathtt{channel} (Fig.~\ref{alg:protocol:close},
      line~\ref{alg:protocol:close:report} and
      Fig.~\ref{alg:protocol:poll:closedch},
      line~\ref{alg:protocol:poll:report}), send (\textsc{closedChannel},
      \forcemathtt{channel}, \alice) to \fpaynet{} and expect (\textsc{continue})
      from \fpaynet{} to resume simulation
      \label{alg:sim:close:report}
    \end{algorithmic}
  \end{simulatorbox}
  \captionof{figure}[Simulator of \fpaynet]{}
  \label{alg:sim:close}
\end{center} \ \\

\begin{lemma}
  \label{lemma:close}
  \begin{gather*}
    \forall k \in \mathbb{N}, \text{ PPT } \environment, \\
    |\Pr[\textsc{Exec}^{\fpaynet{}_{\mathrm{, Pay}},
    \ledger}_{\simulator_{\mathrm{LN - Reg - Open - Pay}}, \environment} = 1] -
    \Pr[\textsc{Exec}^{\fpaynet, \ledger}_{\simulator, \environment} = 1]| \leq
    \\
    nm \cdot \mathrm{E \mhyphen ds}(k) + 3np \cdot \mathrm{E \mhyphen ibs}(k) +
    \\
    nmp \cdot \mathrm{E \mhyphen share}(k) + \mathrm{E \mhyphen prf}(k) + nm
    \cdot \mathrm{E \mhyphen master}(k) \enspace.
  \end{gather*}
\end{lemma}

\begin{proof}
  Like in the previous proof, we here also assume that $\neg P \wedge \neg Q
  \wedge \neg R \wedge \neg S$ holds.

  When \environment{} sends (\textsc{closeChannel}, \forcemathtt{receipt},
  \textit{tid}) to \alice, in the ideal world, if it is not the first closing
  message to \alice{} the message is ignored (Fig.~\ref{alg:fpaynet:close},
  line~\ref{alg:fpaynet:close:noserve}). Similarly in the real world, if there
  has been another such message, \alice{} ignores it
  (Fig.\ref{alg:protocol:close}, lines~\ref{alg:protocol:close:remove}
  and~\ref{alg:protocol:close:ensure}).

  In the case that it is indeed the first closing message, in the ideal world
  \fpaynet{} takes note that this close is pending
  (Fig.~\ref{alg:fpaynet:close},
  lines~\ref{alg:fpaynet:close:retrieve}-\ref{alg:fpaynet:close:mark}) and stops
  serving more requests for this channel (line~\ref{alg:fpaynet:close:noserve}),
  before asking \simulator{} to carry out channel closing. \simulator{} then
  simulates the response to the original message from \environment{} with
  \alice's ITI (Fig.~\ref{alg:sim:close}). Observe that, since \fpaynet{} has
  ensured that this is the first request for closing this particular channel,
  the simulated check of line~\ref{alg:protocol:close:ensure} in
  Fig.~\ref{alg:protocol:close} always passes and the rest of
  Fig.~\ref{alg:protocol:close} is executed. In the real world, the check also
  passes (since we are in the case where this is the first closing message) and
  Fig.~\ref{alg:protocol:close} is executed by the real \alice{} in its
  entirety. Therefore, when \environment{} sends \textsc{closeChannel}, no
  opportunity for distinguishability arises.

  When \environment{} sends (\textsc{getNews}) to \alice, in the ideal world
  \fpaynet{} sends (\textsc{news}, \forcemathtt{newChannels}(\alice),
  \forcemathtt{closedChannels}(\alice), \forcemathtt{updatesToReport}\linebreak(\alice))
  to \environment{} and empties these fields (Fig.~\ref{alg:fpaynet:daemon},
  lines~\ref{alg:fpaynet:getnews}-\ref{alg:fpaynet:getnews:send}). In the real
  world, \alice{} sends (\textsc{news}, \forcemathtt{newChannels},
  \forcemathtt{closedChannels}, \forcemathtt{updatesToReport}) to \environment{} and
  empties these fields as well (Fig.~\ref{alg:protocol:poll},
  lines~\ref{alg:protocol:getnews}-\ref{alg:protocol:getnews:send}).
  \forcemathtt{newChannels}(\alice) in the ideal world is populated in two cases:
  First, when \fpaynet{} receives (\textsc{channelOpened}) after \alice{} has
  previously received (\textsc{checkForNew}) (Fig.~\ref{alg:fpaynet:open},
  line~\ref{alg:fpaynet:channelOpened:report}). This happens when the simulated
  \alice{} ITI handles a \textsc{fundingLocked} message from \bob{}
  (Fig.~\ref{alg:sim:open}, line~\ref{alg:sim:open:channelOpened}). In the real
  world \alice{} would have modified her
  \forcemathtt{new}-\linebreak\forcemathtt{Channels} while handling \bob's
  \textsc{fundingLocked} (Fig.~\ref{alg:protocol:fundingLocked},
  line~\ref{alg:protocol:fundingLocked:report}), thus as far as this case is
  concerned, \forcemathtt{newChannels} has the same contents in the real world as
  does \forcemathtt{newChannels}(\alice) in the ideal. The other case when
  \forcemathtt{newChannels}(\alice) is populated is when \fpaynet{} receives
  (\textsc{fundingLocked}) after \bob{} has previously received
  (\textsc{checkForNew}) (Fig.~\ref{alg:fpaynet:open},
  line~\ref{alg:fpaynet:fundingLocked:report}). This (\textsc{fundingLocked})
  can only be sent by \simulator{} if \alice{} is honest and right before the
  receiving of (\textsc{fundingLocked}) is simulated with her ITI
  (Fig.~\ref{alg:sim:open},
  lines~\ref{alg:sim:fundingLocked:sim:alice}-\ref{alg:sim:fundingLocked:sim:bob}).
  In the real world, \alice's \forcemathtt{newChannels} would be populated upon
  handling the same (\textsc{fundingLocked}). Therefore the \forcemathtt{newChannels}
  part of the message is identical in the real and the ideal world at every
  moment when \environment{} can send (\textsc{getNews}).

  Moving on to \forcemathtt{closedChannels}(\alice), we observe that \fpaynet{} adds
  \forcemathtt{channel} information when it receives (\textsc{closedChannel},
  \forcemathtt{channel}, \alice) from \simulator{} (Fig.~\ref{alg:fpaynet:daemon},
  line~\ref{alg:fpaynet:closedChannel:report}), which in turn happens exactly
  when the simulated \alice{} ITI adds the \forcemathtt{channel} to her
  \forcemathtt{closedChannels} (Fig.~\ref{alg:sim:close},
  line~\ref{alg:sim:close:report}). Therefore it holds that the real and ideal
  \forcemathtt{closedChannels} are always synchronized.

  Regarding \forcemathtt{updatesToReport}, in the real world data is added to it
  exclusively in line~\ref{alg:protocol:pay:raa:report} of
  Fig.~\ref{alg:protocol:pay:revokeAndAck}. In the ideal world on the other
  hand, it is updated in line~\ref{alg:fpaynet:update} of
  Fig.~\ref{alg:fpaynet:pay}, which is triggered only by an (\textsc{update})
  message by \simulator. This message is sent only when
  line~\ref{alg:protocol:pay:raa:report} of
  Fig.~\ref{alg:protocol:pay:revokeAndAck} is simulated by \simulator{}
  (Fig.~\ref{alg:sim:pay}, line~\ref{alg:sim:push:report}). In the real world,
  this happens only after receiving a valid (\textsc{revokeAndAck}) message from
  the channel counterparty and after first having sent a corresponding
  (\textsc{commitmentSigned}) message (Fig.~\ref{alg:protocol:pay:revokeAndAck},
  line~\ref{alg:protocol:pay:revokeAndAck:ensure} and
  Fig.~\ref{alg:protocol:pay:commitmentSigned},
  lines~\ref{alg:protocol:pay:commit:nomark}
  and~\ref{alg:protocol:pay:commit:mark}), which happens only after receiving
  (\textsc{commit}) from \environment. In the ideal world a simulation of the
  same events can only happen in the exact same case, i.e.\ when \environment{}
  sends an identical (\textsc{commit}) to the same player. Indeed, \fpaynet{}
  simply forwards this message to \simulator{} (Fig.~\ref{alg:fpaynet:daemon},
  line~\ref{alg:fpaynet:daemon:commit}), who in turn simply simulates the
  response to the message with the simulated ITI that corresponds to the player
  that would receive the message in the real world (Fig.~\ref{alg:sim:pay},
  line~\ref{alg:sim:push:commit}). We conclude that the \forcemathtt{updatesToReport}
  sent to \environment{} in either the real or the ideal world are always
  identical.

  Lastly, in the ideal world, whenever (\textsc{read}) is sent to \ledger{} and
  a reply is received, the function \forcemathtt{checkClosed}
  (Fig.\ref{alg:fpaynet:close:func}) is called with the reply of the \ledger{}
  as argument. This function does not generate new messages, but may cause the
  \fpaynet{} to halt. We will now prove that this never happens.

  \fpaynet{} halts in line~\ref{alg:fpaynet:close:func:dsforgery} of
  Fig.~\ref{alg:fpaynet:close:func} in case a channel is closed without using a
  commitment transaction. Similarly to event $E$ in the proof of
  Lemma~\ref{lemma:pay}, this event is a subset of $P$ and thus is impossible to
  happen given that we assume $\neg P$.

  \fpaynet{} halts in line~\ref{alg:fpaynet:close:func:malicious} of
  Fig.~\ref{alg:fpaynet:close:func} in case a malicious closure by the
  counterparty was successful, in spite of the fact that \alice{} polled in time
  to apply the punishment. A (\textsc{poll}) message to \alice{} within the
  prescribed time frame (line~\ref{alg:fpaynet:close:func:ifmalicious}) would
  cause \fpaynet{} to alert \simulator{} (Fig.~\ref{alg:fpaynet:poll},
  line~\ref{alg:fpaynet:poll:send}), who in turn would submit the punishment
  transaction in time to prevent the counterparty from spending the delayed
  payment (Fig.~\ref{alg:protocol:poll:closedch},
  lines~\ref{alg:protocol:poll:mal:tx}-\ref{alg:protocol:poll:mal:submit}).
  Therefore the only way for a malicious counterparty to spend the delayed
  output before \alice{} has the time to punish is by spending the punishment
  output themself. This however can never happen, since this event would be a
  subset of either $R$, if $\mathtt{remoteCom}_n$ (i.e.\ the counterparty closed
  the channel) is in $\Sigma_{\alice}$, or $Q$, if $\mathtt{localCom}_n$ is in
  $\Sigma_{\alice}$ (i.e.\ \alice{} closed the channel).

  \fpaynet{} halts in line~\ref{alg:fpaynet:close:func:idle} of
  Fig.~\ref{alg:fpaynet:close:func} in case \environment{} has asked for the
  channel to close, but too much time has passed since. This event cannot
  happen, for two reasons. First, regarding elements in
  \forcemathtt{pendingClose}(\alice), because \fpaynet{} forwards to \simulator{} a
  (\textsc{closeChannel}) message (Fig.~\ref{alg:fpaynet:close},
  line~\ref{alg:fpaynet:close:send}) for each element that it adds to
  \forcemathtt{pendingClose} (Fig~\ref{alg:fpaynet:close},
  line~\ref{alg:fpaynet:close:mark}) and this causes \simulator{} to submit the
  closing transaction to \ledger{} (Fig.~\ref{alg:protocol:close},
  line~\ref{alg:protocol:close:submit}). This transaction is necessarily valid,
  because there is no other transaction that spends the funding transaction of
  the channel, according to the first check of
  line~\ref{alg:fpaynet:close:func:ifidle} of Fig.~\ref{alg:fpaynet:close:func}.
  \fpaynet{} halts in this case only if it is sure that the chain has grown by
  \tochain{} blocks, and thus if the closing transaction had been submitted when
  (\textsc{closeChannel}) was received, it should have been necessarily included
  (Proposition~\ref{prop:tochain}). Second, every element added to
  \forcemathtt{closedChannels} (Fig.~\ref{alg:protocol:close},
  line~\ref{alg:protocol:close:report} and
  Fig.~\ref{alg:protocol:poll:closedch}, line~\ref{alg:protocol:poll:report})
  corresponds to a submission of a closing transaction for the same channel
  (Fig.~\ref{alg:protocol:close}, line~\ref{alg:protocol:close:submit}), or to a
  channel for which the closing transaction is already in the ledger state
  (Fig.~\ref{alg:protocol:poll:closedch},
  line~\ref{alg:protocol:poll:closedch:loop}). In both cases, the transaction
  has been submitted at least \tochain{} blocks earlier, thus again by
  Proposition~\ref{prop:tochain} it is impossible for the transaction not to be
  in the ledger state. Therefore \fpaynet{} cannot halt in
  line~\ref{alg:fpaynet:close:func:idle} of Fig.~\ref{alg:fpaynet:close:func}.
  We deduce that, given $\neg P \wedge \neg Q \wedge \neg R$, the execution of
  \forcemathtt{checkClosed} by \fpaynet{} does not contribute any increase to the
  probability of distinguishability. Put otherwise, given $\neg P \wedge \neg Q
  \wedge \neg R$, it is $\textsc{Exec}^{\fpaynet{}_{\mathrm{, Pay}},
  \ledger}_{\simulator_{\mathrm{LN - Reg - Open - Pay}}, \environment} =
  \textsc{Exec}^{\fpaynet, \ledger}_{\simulator, \environment}$.

  \fpaynet{} halts in line~\ref{alg:fpaynet:close:func:balance} of
  Fig.~\ref{alg:fpaynet:close:func} in case all \alice's channels are closed
  on-chain and either \alice's off-chain balance is not equal to zero, or if her
  on-chain balance is not the expected one, as reported by \simulator. This
  event can never happen for the following reasons. Firstly, as we have seen,
  \simulator{} reports all updates with an (\textsc{update}) message
  (Fig.~\ref{alg:sim:pay}, line~\ref{alg:sim:push:report}) and a
  (\textsc{resolvePays}) message; upon receiving the latter and given that it
  doesn't halt, \fpaynet{} updates offChainBalance(\alice) if she is the payer
  or payee of one of the resolved payments (Fig.~\ref{alg:fpaynet:resolvepay},
  lines~\ref{alg:fpaynet:resolvepay:corr:credit},~\ref{alg:fpaynet:resolvepay:debit}
  and~\ref{alg:fpaynet:resolvepay:credit}). Secondly, upon closure of each
  channel, \fpaynet{} would have halted if the closing balance were not the
  expected one (Fig.~\ref{alg:fpaynet:close:func},
  line~\ref{alg:fpaynet:close:func:ifmalicious}), an event that cannot happen as
  we have already proven. Lastly, upon each channel opening and closing,
  \fpaynet{} updates offChainBalance(\alice) and onChainBalance(\alice) to
  reflect the event (Fig.~\ref{alg:fpaynet:open},
  lines~\ref{alg:fpaynet:channelOpened:offchain}
  and~\ref{alg:fpaynet:channelOpened:onchain} and
  Fig.~\ref{alg:fpaynet:close:func},
  lines~\ref{alg:fpaynet:close:func:alice:credit}
  or~\ref{alg:fpaynet:close:func:bob:credit} respectively). Therefore, it is
  impossible for \fpaynet{} to halt here.

  Similarly to the previous proof, if we allow for forgeries again, i.e.\ if we
  allow the event $P \vee Q \vee R \vee S$, we observe that $\Pr[P \vee Q \vee R
  \vee S] \leq nm \cdot \mathrm{E \mhyphen ds}(k) + 3np \cdot \mathrm{E \mhyphen
  ibs}(k) + nmp \cdot \mathrm{E \mhyphen share}(k) + \mathrm{E \mhyphen prf}(k)
  + nm \cdot \mathrm{E \mhyphen master}(k)$, where $n$ is the number of players,
  $m$ is the maximum channels a player can open and $p$ is the maximum number of
  updates a player can perform. We thus deduce that
  \begin{gather*}
    \forall k \in \mathbb{N}, \text{ PPT } \environment, \\
    |\Pr[\textsc{Exec}^{\fpaynet{}_{\mathrm{, Pay}},
    \ledger}_{\simulator_{\mathrm{LN - Reg - Open - Pay}}, \environment} = 1] -
    \Pr[\textsc{Exec}^{\fpaynet, \ledger}_{\simulator, \environment} = 1]| \leq
    \\
    nm \cdot \mathrm{E \mhyphen ds}(k) + 3np \cdot \mathrm{E \mhyphen ibs}(k) +
    \\
    nmp \cdot \mathrm{E \mhyphen share}(k) + \mathrm{E \mhyphen prf}(k) + nm
    \cdot \mathrm{E \mhyphen master}(k) \enspace.
  \end{gather*}
\end{proof}
