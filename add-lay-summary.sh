set -e
pdftk thesis.pdf cat 1 output title.pdf
pdftk thesis.pdf cat 3-end output no-title-thesis.pdf
pdfunite title.pdf laysummary.pdf no-title-thesis.pdf full-thesis.pdf
rm no-title-thesis.pdf title.pdf
