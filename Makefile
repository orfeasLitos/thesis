TIKZS = $(patsubst src/figures/dot/%.dot, src/figures/auto-tikz/%.tex, $(wildcard src/figures/dot/*.dot))

all: figures thesis.pdf

#.ONESHELL:
thesis.pdf: $(shell find ./src/ -print)
	export TEXINPUTS=.:./src//:; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode thesis.tex; \
	rm -rf thesis.aux thesis.log thesis.out thesis.toc thesis.lof thesis.lot thesis.bbl thesis.blg thesis-autopp.out thesis-pics.pdf thesis-autopp.log thesis-autopp.xcp

bib: $(shell find ./src/ -print)
	export TEXINPUTS=.:./src//:; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode thesis.tex; \
	bibtex thesis.aux; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode thesis.tex; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode thesis.tex; \
	rm -rf thesis.aux thesis.log thesis.out thesis.toc thesis.lof thesis.lot thesis.bbl thesis.blg thesis-autopp.out thesis-pics.pdf thesis-autopp.log thesis-autopp.xcp

figures: $(TIKZS)

src/figures/manual-tikz/*:

src/figures/auto-tikz/%.tex: src/figures/dot/%.dot
ifeq (, $(shell which dot2tex))
	
else
	dot2tex --texmode math --format tikz --figonly --autosize --usepdflatex --nominsize --prog dot $< > $@
endif

clean:
	rm -rf *.aux *.log *.out *.toc *.lof *.lot *.bbl *.blg *.pdf src/figures/auto-tikz/*.tex
